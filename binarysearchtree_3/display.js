/*
Name of the project : Datastructure_binarysearchtree
File name : binarysearchtree_3/display.js
Description : merge to given binary serach tree and print the nodes in soerted
              order
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,5,2,3,7,4,6,8,10,9],[11,15,12,14,19,17,18,13,20,16]
Output : 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
*/


class Node {
    constructor(data) {
		    this.data = data;
		    this.left = null;
		    this.right = null;
	  }
}


class BinarySearchTree
{
    constructor() {
       // root of a binary seach tree
       this.root = null;
    }
    insert(data) {
	      var newNode = new Node(data);
	      if(this.root === null)
		        this.root = newNode;
	      else
		        this.insertNode(this.root, newNode);
    }
		//function to add nodes into the bst
    insertNode(node, newNode) {
	      if(newNode.data < node.data) {
		        if(node.left === null)
			          node.left = newNode;
		        else
			          this.insertNode(node.left, newNode);
	      }else {
	    	    if(node.right === null)
		   	        node.right = newNode;
	     	    else
			          this.insertNode(node.right,newNode);
	      }
     }
		 //function to traverse the tree in inorder
     inorder(node) {
         if(node != null) {
             this.inorder(node.left);
						 console.log(node.data);
						 this.inorder(node.right);
         }
     }
		 //function merge two trees
     mergeTree(node) {
         if(node != null) {
             BST1.insert(node.data);
             this.mergeTree(node.left);
             this.mergeTree(node.right);
         }
     }
     getRootNode() {
	       return this.root;
     }
}


var BST1 = new BinarySearchTree();
var BST2 = new BinarySearchTree();
function display(arr1,arr2) {
    for (var i = 0; i < arr1.length; i++) {
        BST1.insert(arr1[i]);
    }
    for (var i = 0; i < arr2.length; i++) {
        BST2.insert(arr2[i]);
    }
		var root2 = BST2.getRootNode();
    BST2.mergeTree(root2);
		var root1 = BST1.getRootNode();
    BST1.inorder(root1);
}


display([1,5,2,3,7,4,6,8,10,9],[11,15,12,14,19,17,18,13,20,16]);
