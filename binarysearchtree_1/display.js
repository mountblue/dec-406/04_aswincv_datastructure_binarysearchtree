/*
Name of the project : Datastructure_binarysearchtree
File name : binarysearchtree_1/display.js
Description : prints the array in spiral form
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5,6,7,8]
Output : 4
*/



var level;
var flag = true;
function display(tree) {
      level = Math.floor(Math.log2(tree.length)) ;
      var cnt = count(tree,0);
      if(flag) {
          console.log("true");
      }else {
          console.log("false");
      }
      console.log(cnt);
}


function count(tree, itr) {
    if(tree[itr]==null){
        if(itr<2**level+1) {
            flag = false;
        }
        return 0;
    }
    var count1=0,count2=0;
    if(2*itr+1<tree.length) {
        count1 = count(tree,2*itr+1);
    }
    if (2*itr+2<tree.length) {
        count2 = count(tree,2*itr+2);
    }
    if(count1==0 && count2==0 ) {
        return 1;
    }else {
        return( (count1>count2 ? count1 : count2)+1 );
    }
}

display([1,2,3,4,5,6,7,8]);
