/*
Name of the project : Datastructure_binarysearchtree
File name : binarysearchtree_4/display.js
Description : program checks whether a given tree is binary search tree or not
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
*/



class Node
{
	constructor(data)
	{
		this.data = data;
		this.left = null;
		this.right = null;
	}
}
class BinarySearchTree
{
    constructor() {
       // root of a binary seach tree
       this.root = null;
    }
    insert(data) {
	      var newNode = new Node(data);
	      if(this.root === null)
		        this.root = newNode;
	      else
		        this.insertNode(this.root, newNode);
    }
    insertNode(node, newNode) {
	      if(newNode.data < node.data) {
		        if(node.left === null)
			          node.left = newNode;
		        else
			          this.insertNode(node.left, newNode);
	      }else {
	    	    if(node.right === null)
		   	        node.right = newNode;
	     	    else
			          this.insertNode(node.right,newNode);
	      }
     }
     // searching starts from given node
     findMinNode(node) {
         if(node.left === null)
             return node;
         else
             return this.findMinNode(node.left);
     }
     findMaxNode(node) {
       if(node.right === null)
           return node;
       else
           return this.findMinNode(node.right);
     }
     isBinaryTree(node) {
           if(node.left == null && node.right == null) {
               return true;
           }else if (node.left!=0 && node.data<findMaxNode(node.left)) {
               return false;
           }else if (node.right!=0 && node.data>findMinNode(node.right)) {
               return false;
           }else {
               if(!isBinaryTree(node.left) || !isBinaryTree(node.right))
               return false;
           }
           return true;
     }
}
