/*
Name of the project : Datastructure_binarysearchtree
File name : binarysearchtree_2/display.js
Description : delete a node with specified key
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,5,2,3,7,4,6,8,10,9]
Output : 4 , 7 , 2
*/


class Node
{
	constructor(data)
	{
		this.data = data;
		this.left = null;
		this.right = null;
	}
}
class BinarySearchTree
{
    constructor() {
       // root of a binary seach tree
       this.root = null;
    }
    insert(data) {
	      var newNode = new Node(data);
	      if(this.root === null)
		        this.root = newNode;
	      else
		        this.insertNode(this.root, newNode);
    }
    insertNode(node, newNode) {
	      if(newNode.data < node.data) {
		        if(node.left === null)
			          node.left = newNode;
		        else
			          this.insertNode(node.left, newNode);
	      }else {
	    	    if(node.right === null)
		   	        node.right = newNode;
	     	    else
			          this.insertNode(node.right,newNode);
	      }
     }
     remove(data) {
         this.root = this.removeNode(this.root, data);
     }
     removeNode(node, key) {
	       if(node === null) {
		         return null;
         }else if(key < node.data) {
		         node.left = this.removeNode(node.left, key);
		         return node;
	       }else if(key > node.data) {
		         node.right = this.removeNode(node.right, key);
		         return node;
	        }else {
              console.log(node.data);
              //if there is no child
		          if(node.left === null && node.right === null) {
			            node = null;
			            return node;
	      	    }
              //if there is only one child
		          if(node.left === null) {
			            node = node.right;
			            return node;
		          }else if(node.right === null) {
			            node = node.left;
			            return node;
              }
              //if the node have 2 child
		          var aux = this.findMinNode(node.right);
		          node.data = aux.data;
              node.right = this.removeNode(node.right, aux.data);
		          return node;
	         }
     }
     // searching starts from given node
     findMinNode(node) {
         if(node.left === null)
             return node;
         else
             return this.findMinNode(node.left);
     }
}


function display(arr) {
    var BST = new BinarySearchTree();
    for (var i = 0; i < arr.length; i++) {
      BST.insert(arr[i]);
    }
    BST.remove(4);
    BST.remove(7);
    BST.remove(2);
}


display([1,5,2,3,7,4,6,8,10,9]);
